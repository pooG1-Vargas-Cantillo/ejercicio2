package edu.unimagdalena.app;

import edu.unimagdalena.academico.Alumno;

public class Aplicacion {

	public static void main(String[] args) {
	 Alumno a = new Alumno("juan",18);
	 a.agregarAsignatura("caculo",300);
	 a.agregarAsignatura("mecanica",400);
	 a.agregarAsignatura("poo",250);
	 
	 Alumno b= new Alumno("pedro",30); 
	 b.agregarAsignatura("caculo",300);
	 b.agregarAsignatura("mecanica",450);
	 b.agregarAsignatura("poo",500);
	 
	 Alumno c= new Alumno("maria",17); 
	 c.agregarAsignatura("caculo",500);
	 c.agregarAsignatura("mecanica",450);
	 c.agregarAsignatura("poo",450);
	 
	 
	 a.imprimir();
	 b.imprimir();
	 c.imprimir();
	 
	 

	}

}
