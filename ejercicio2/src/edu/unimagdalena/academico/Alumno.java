package edu.unimagdalena.academico;

import java.util.ArrayList;

public class Alumno {
	private int edad;
	private String nombre;
	private  ArrayList<Asignatura> asignatura;
	

	public Alumno(String nombre, int edad)
	{
		this.nombre = nombre;
		this.edad = edad;
		asignatura= new ArrayList<Asignatura>(); 
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}
	 public int promedioAlumno()
	 {
		  int acum =0;
		  int nAsignaturas=0;
		  int promedioAlumno;
		 
		 
		 for(Asignatura Asg: asignatura)
			{
				acum += Asg.getNota();
				nAsignaturas++;
				
			}
		 promedioAlumno=acum/nAsignaturas;
		 return promedioAlumno;
	 }
	 
	 public void agregarAsignatura(String nombreA, int nota)
	 {
		 boolean sw=false;
		 for(Asignatura Asg: asignatura)
			{
				 if(Asg.getNombre().equals(nombreA))
				 {
					 sw=true;
				 }
			}
		 	if(sw==false)
		 	{
		 		Asignatura asgt = new Asignatura(nota,nombreA);
		 		asignatura.add(asgt);
		 		
		 	}
		
	 }
	 public void imprimir()
	 {
		 System.out.println("le alumno: "+getNombre());
		 System.out.println("Edad: "+getEdad());
		 for(Asignatura Asg: asignatura)
			{
			
			 System.out.println("asigantura: "+Asg.getNombre());
			 System.out.println("nota: "+Asg.getNota());
			 System.out.print("Estado: ");Asg.calificar();
			 
			 System.out.println("============================");
			}
		 System.out.println("promedio: "+promedioAlumno());
		 System.out.println();
		 System.out.println();
		 System.out.println();
	 }
	
	

}
