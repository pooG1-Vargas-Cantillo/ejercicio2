package edu.unimagdalena.academico;

public class Asignatura {
	private int nota;
	private String nombre;
	
	Asignatura(int nota,String nombre)
	{
		this.nombre = nombre;
		this.nota = nota;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}
	public void calificar()
	{
		if(nota >= 60)
		{
			System.out.println("Aprobado");
		}
		else
			System.out.println("reprobado");
	}

	public String getNombre() {
		return nombre;
	}
	
	

}
